# $(call subst_all,<from>,<to>,<text>) ============================================================================
#
# Replaces all <from> strings with <to> in <text> like $(subst <from>,<to>,<text>), but calls itself recursively
# until no no more replacements can be performed (e.g. $(call subst_all,//,/,/////) will result in a single slash).
#
subst_all = $(if $(filter-out $3,$(subst $1,$2,$3)),$(call subst_all,$1,$2,$(subst $1,$2,$3)),$3)

# $(call patsubst_all,<pattern>,<replacement>,<text>) =============================================================
#
# Like subst_all, but for patsubst instead of subst.
#
patsubst_all = $(if $(filter-out $3,$(patsubst $1,$2,$3)),$(call patsubst_all,$1,$2,$(patsubst $1,$2,$3)),$3)

# $(call filter_file,<pattern>,<file>) ============================================================================
#
# If <pattern> ends with a slash and <file> is a directory, returns <file> with a slash appended (this may lead to
# multiple slashes at the end if <file> already ends with a slash). If <pattern> does not end with a slash and
# <file> is not a directory, returns <file>. In all other cases returns an empty value. The return value for
# anything that is not a regular file or directory is unspecified, for non-existing files or directories it is an
# empty value. Warning: If <file> contains wildcard placeholders (*, ?) results may be garbage.
#
filter_file = $(if $(filter %/,$1),$(if $(wildcard $2/.),$2/),$(if $(wildcard $2/.),,$(if $(wildcard $2),$2)))

# $(call find_simple,<directory>,<pattern>) =======================================================================
#
# Returns a list of all files (and directories) beneath the <directory> (recursively including subdirectories)
# matching the <pattern>. If the <pattern> ends with a slash, entries will be restricted to (sub-)directories, and
# the trailing slash will be included in the results. Otherwise, no trailing slash will be included, even for
# entries that are in fact directories. Hidden files and directories will not be included, unless the pattern
# starts with a "." (warning: this will also match all . and .. entries of regular directories, but will not
# recurse into hidden subdirectories). The elements returned may contain multiple consecutive slashes. Do not
# directly call this function (use one where it doesn't say this)!
#
find_simple = $(wildcard $1/$2) $(foreach dir,$(wildcard $1/*/),$(call find_simple,$(dir),$2))

# $(call find_better,<directory>,<pattern>) =======================================================================
#
# Like find_simple, but . and .. entries will be filtered out (only at the end of the path). It is suggested to use
# find_in_dir instead of find_better.
#
find_better = $(filter-out %/. %/./ %/.. %/../,$(call find_simple,$1,$2))

# $(call find_f_or_d,<directory>,<pattern>) =======================================================================
#
# Like find_better, but results will be restricted to either files or directories: if the <pattern> ends with a
# slash, only matching directories will be returned, as is the case with find_better, but - unlike find_better -
# find_f_or_d will only return matching files if the <pattern> does not end with a slash. Results may still contain
# multiple consecutive slashes, it is recommended to use find_in_dir instead.
#
find_f_or_d = $(foreach elem,$(call find_better,$1,$2),$(call filter_file,$2,$(elem)))

# $(call find_in_dir,<directory>,<pattern>) =======================================================================
#
# Like find_f_or_d, but multiple consecutive slashes will be replaced by a single one. Additionally, all leading ./
# components will be removed.
#
find_in_dir = $(call patsubst_all,./%,%,$(call subst_all,//,/,$(call find_f_or_d,$1,$2)))

# $(call default_val,<value>,<default>) ===========================================================================
#
# Returns <value> if it is not empty, <default> otherwise. Note that all whitespace is not empty, and leading and
# trailing spaces will be preserved for both the <value> and the <default>!
#
default_val = $(if $1,$1,$2)

# $(call parent_dirs,<path>) ======================================================================================
#
# Returns a (depth first) list of all parent directories of <path> up to the current directory (for any relative
# <path>), or up to the root directory (for any absolute <path>). If <path> ends with "/", <path> it will be
# included in the list, otherwise it will be omitted (even if it is actually a directory in the filesystem). If
# <path> is empty, the list will contain the current directory. All paths in the list will end with a slash. Note:
# This function does not perform any path resolution or filesystem lookup, this is purely string processing. Stuff
# like "." and ".." will be treated like any other string in <path>. Multiple consecutive slashes will be handled
# correctly, though. <path>s containing whitespace are not supported!
#
parent+dirs = $(if $(filter-out .,$1),$(dir $1) $(call parent+dirs,$(patsubst %/,%,$(dir $1))))
parent_dirs = $(strip $(call default_val,$(call parent+dirs,$(call subst_all,//,/,$1)),./))

# $(call first_match,<path>,<pattern>) ============================================================================
#
# Looks for <pattern> in <path> (may be a file or directory, see parent_dirs) and all its parent directories (depth
# first, until the current directory is reached for relative paths, or the root directory is reached for absolute
# paths), and returns the first matching file (or directory, depending on whether <pattern> ends with a slash or
# not), if any, or an empty string if no matches could be found. Note that <pattern> must not contain wildcards, it
# must be the name of a file or directory!
#
first+match = $(firstword $(foreach dir,$(call parent_dirs,$1),$(call filter_file,$2,$(wildcard $(dir)$2))))
first_match = $(call subst_all,//,/,$(call first+match,$1,$2))

# $(call find_common,<path>,<files>) ==============================================================================
#
# Searches for the specified <files>, starting at <path> (which may be a directory or a file in the directory to
# start the search), and in all its parent directories, up to the current directory if <path> is a relative path or
# the root directory if it is an absolute path. The first match for every file will be returned, i.e. the match
# closest to the <path>.
#
find_common = $(foreach file,$2,$(call first_match,$1,$(file)))

#==================================================================================================================

letters_dir = letters
common_deps = letteroptions.lco senderdata.lco

common_args = --keep-intermediates --print
search_path = $(foreach path,$(call parent_dirs,$1),-Z 'search-path=$(patsubst %/,%,$(path))')

letters_pdf = $(patsubst %.tex,%.pdf,$(call find_in_dir,$(letters_dir),*.tex))
other_files = $(filter-out $1,$(call find_in_dir,$(dir $1),*))
letter_deps = $$(call other_files,$$@)

distr_files = $(wildcard *.cls) $(wildcard *.lco) $(wildcard *.sty)

rm_suffixes = aux dvi idx ilg ind gz lof log lot out ps toc tmp
rm_examples = doc letters/examples/commented letters/examples/minimal
clean_files = $(foreach path,$(rm_examples),$(foreach ext,$(rm_suffixes),$(path)/*.$(ext)))

moredeps ?= 0
ifeq ($(moredeps),1)
   letter_deps += $$(call find_common,$$@,$(common_deps))
else ifeq ($(moredeps),2)
   letter_deps += $$(call find_common,$$@,$(common_deps)) $(distr_files)
else
   letter_deps += $$(if $$(filter $$@,$(MAKECMDGOALS)),$$(call find_common,$$@,$(common_deps)))
endif

.PHONY: letters
letters: $(letters_pdf)

.SECONDEXPANSION:
letters/%.pdf: $(letter_deps)
	tectonic $(common_args) $(call search_path,$@) $(@:pdf=tex)

.PHONY: doc
doc: doc/cstmlttr.pdf

doc/cstmlttr.pdf: $(letter_deps) $(distr_files) doc/cmd.ind doc/len.ind doc/opt.ind doc/var.ind
	tectonic $(common_args) $(call search_path,$@) '$(@:pdf=tex)'
	@ printf '\n'
	@ printf 'NOTE: Multiple runs may be required in order to get the correct output!\n'
	@ printf 'NOTE: Run `make doc` again if the index or TOC is missing or incorrect!\n'
	@ printf '\n'

doc/%.ind: doc/%.idx
	if test -f '$<' ; then makeindex -s doc/cstmlttr.ist -L '$<' ; fi

doc/%.idx: doc/cstmlttr.tex
	tectonic $(common_args) $(call search_path,$@) '$<'

.PHONY: clean
clean:
	rm -f $(clean_files)

.PHONY: cleaner
cleaner: clean
	rm -f doc/cstmlttr.pdf letters/examples/commented/commented.pdf letters/examples/minimal/minimal.pdf

###:indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=115:##############################################

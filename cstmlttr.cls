%% cstmlttr.cls
%% Copyright 2021-2022 Stefan Göbel
%
% This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either
% version 1.3 of this license or (at your option) any later version.
%
% The latest version of this license is in <http://www.latex-project.org/lppl.txt> and version 1.3 or later is part
% of all distributions of LaTeX version 2005/12/01 or later.
%
% This work consists of the files cstmlttr.cls and cstmlttr.sty.

\NeedsTeXFormat{LaTeX2e}[2020-02-02]
\ProvidesClass{cstmlttr}[2022/01/23 v0.4]

\LoadClassWithOptions{scrlttr2}
\RequirePackageWithOptions{cstmlttr}

\endinput

%%:indentSize=3:tabSize=3:noTabs=true:mode=latex:maxLineLen=115:%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
